# RAM_2 README #
This repo covers 3 projects:

1. ss - Shared Services Project
1. NIEM - PGA message set from CBP
1. pilot_info - documents from the original RAM project

# Shared Services Project #

## Technologies ##

 * MS Sql Server
 * JBoss Wildfly 9.0.2.Final
 * Maven
 * Spring
    * Core Context
    * MVC - Restful services
    * Java Configuration
    * Data Repositories
    * JPA 2.1 - Hibernate 4.3
 * Java EE 7
 * Log4J2 - writing to a DB table
 * Freemarker 2.3 - Email templating

## Features ##

### Logging ###

Regular log4j logging and restful endpoint for logging

Creats a log with a custom message associated with the provided userId, on the log level designated by the API path:
```
POST http://localhost:8080/ss/api/logs/[fatal|error|warn|info|debug]
{"userId":"dlauber","message":"this is a test"}
```

### Email ###

restful endpoints for sending a basic email and another for a specific template

For a regular email:

```
POST http://localhost:8080//ss/api/email/send
{"toEmailAddress":"mwilmoth@ntelx.com","subject":"test email","body":"this is a test","sendHtml":false}
```

For a Supply chain email:

```
POST http://localhost:8080//ss/api/email/supplychain
{"listEmailAddresses":["rzauel@ntelx.com"],"listSupplyChainEmailDataDto":[{"entryNo":"e123","lineNo":"l123","workflowStatusDesc":"w123","portId":"pi123","portName":"pn123"}]}
```

## Setup ##

* Setup files are stored in the setup directory: /git-root/ss/setup.
* There is directory db contains scripts for creating the database schema and initial loading of tables
* This is a document wildfly-setup.txt which explains how to configure the driver and datasource in the wildfly standalone.xml and where to put the jdbc files in the module directory
* There are files in the jdbc directory used to setup the wildfly database driver jtds-1.3.1.jar