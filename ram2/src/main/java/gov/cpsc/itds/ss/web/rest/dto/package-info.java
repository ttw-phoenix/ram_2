/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package gov.cpsc.itds.ss.web.rest.dto;
