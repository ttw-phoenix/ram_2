-- ram2_user
CREATE LOGIN ram2_user WITH PASSWORD='password', DEFAULT_LANGUAGE=us_english, CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;
CREATE USER ram2_user FOR LOGIN ram2_user WITH DEFAULT_SCHEMA=ram2;
--EXEC sp_addrolemember N'db_ddladmin', N'ram2_user';
EXEC sp_addrolemember N'db_datareader', N'ram2_user';
EXEC sp_addrolemember N'db_datawriter', N'ram2_user';

-- ram2_owner
CREATE LOGIN ram2_owner WITH PASSWORD='password', DEFAULT_LANGUAGE=us_english, CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;
CREATE USER ram2_owner FOR LOGIN ram2_owner WITH DEFAULT_SCHEMA=ram2;
EXEC sys.sp_addsrvrolemember @loginame = N'ram2_owner', @rolename = N'dbcreator';
EXEC sp_addrolemember N'db_accessadmin', N'ram2_owner';
EXEC sp_addrolemember N'db_datareader', N'ram2_owner';
EXEC sp_addrolemember N'db_datawriter', N'ram2_owner';

ALTER LOGIN ram2_user WITH PASSWORD=N'nin0!Cpsc';
ALTER LOGIN ram2_owner WITH PASSWORD=N'nin@1Cpsc';