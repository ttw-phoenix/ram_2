create table ram2.system_property (
    property_key     varchar(255) primary key not null,
    property_value   varchar(4000)
);

create table ram2.log (
    log_id          bigint IDENTITY(1,1) primary key not null,
    user_id         varchar(20),
    date_created    datetime not null,
    logger_name     varchar(50) not null,
    level           varchar(10) not null,
    message         varchar(255) not null,
    wrappedEvent    varchar(50)
);
