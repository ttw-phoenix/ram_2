insert into ram2.system_property (property_key, property_value) values
    ('email_activated','true');

insert into ram2.system_property (property_key, property_value) values
    ('email_host','mx1.ntelx.net');

insert into ram2.system_property (property_key, property_value) values
    ('email_port','25');

insert into ram2.system_property (property_key, property_value) values
    ('email_from','ramcheck@ntelx.com');

insert into ram2.system_property (property_key, property_value) values
    ('deact_user_days_back','90');

