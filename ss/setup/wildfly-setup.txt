###############################################################################
## modify standalone.xml
## add datasource and driver
###############################################################################
jta="false" 
use-ccm="false"
jndi-name="java:jboss/datasources/ram2ds" 
pool-name="ram2ds" 
###############################################################################

<subsystem xmlns="urn:jboss:domain:datasources:2.0">
    <datasources>
        <datasource jta="false" jndi-name="java:jboss/datasources/ram2ds" pool-name="ram2ds" enabled="true" use-ccm="false">
            <connection-url>jdbc:jtds:sqlserver://cpsc01.ntelx.net:1190/CPSC;instance=SQLEXPRESS</connection-url>
            <driver-class>net.sourceforge.jtds.jdbc.Driver</driver-class>
            <driver>JTDS</driver>
            <security>
                <user-name>ram2_user</user-name>
                <password>nin0!Cpsc</password>
            </security>
        </datasource>
        <drivers>
            <driver name="JTDS" module="net.sourceforge.jtds">
                <driver-class>net.sourceforge.jtds.jdbc.Driver</driver-class>
            </driver>
        </drivers>
    </datasources>
</subsystem>

###############################################################################
## add jdbc driver jar files to wildfly:
###############################################################################
mkdir <wildfly-9.0.2.Final>\modules\net\sourceforge\jtds\main
cd <wildfly-9.0.2.Final>\modules\net\sourceforge\jtds\main
copy jtds-1.3.1.jar to folder
copy module.xml to folder
###############################################################################
