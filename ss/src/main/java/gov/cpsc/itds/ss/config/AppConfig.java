package gov.cpsc.itds.ss.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author rzauel
 */
@Configuration
@ComponentScan(basePackages = "gov.cpsc.itds.ss")
public class AppConfig {
    
}
