package gov.cpsc.itds.ss.config;

import java.util.HashMap;
import java.util.Map;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;

/**
 *
 * @author rzauel
 */
@Configuration
@EnableJpaRepositories("gov.cpsc.itds.ss.repository")
@EnableTransactionManagement
public class DatabaseConfiguration {

    private final Logger log = LogManager.getLogger(DatabaseConfiguration.class);

    /**
     *
     * @return
     */
    @Bean
    public EntityManager entityManager() {
        return entityManagerFactory().createEntityManager();
    }

    /**
     *
     * @return
     */
    @Bean
    public EntityManagerFactory entityManagerFactory() {
        HibernatePersistenceProvider hibernatePersistenceProvider = new HibernatePersistenceProvider();
        Map properties = new HashMap<>();
        EntityManagerFactory emf = hibernatePersistenceProvider.createEntityManagerFactory("ram2ds", properties);
        return emf;
    }

    /**
     *
     * @return
     */
    @Bean
    public JpaDialect jpaDialect() {
        return new HibernateJpaDialect();
    }

    /**
     *
     * @return
     */
    @Bean
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager(entityManagerFactory());
        return transactionManager;
    }

}
