package gov.cpsc.itds.ss.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author rzauel
 */
@EnableWebMvc
@Configuration
public class WebMvcConfig {

}
