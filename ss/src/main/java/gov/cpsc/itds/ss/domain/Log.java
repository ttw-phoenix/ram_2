package gov.cpsc.itds.ss.domain;

import gov.cpsc.itds.ss.dto.LogDto;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.db.jpa.BasicLogEventEntity;
import org.apache.logging.log4j.message.Message;

/**
 * 
 * @author rzauel
 */
@Entity
@Table(name = "log", schema = "ram2")
@NamedQueries({
    @NamedQuery(name = "Log.findAll", query = "SELECT l FROM Log l"),
    @NamedQuery(name = "Log.findByLogId", query = "SELECT l FROM Log l WHERE l.logId = :logId")})
public class Log extends BasicLogEventEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "log_id")
    private Long logId;

    @Size(max = 20)
    @Column(name = "user_id")
    private String userId;

    @NotNull
    @Column(name = "date_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated = new Date();

    @Basic
    @Size(min = 1, max = 10)
    @NotNull
    @Column(name = "level")
    private String levelName;

    @Basic
    @Size(min = 1, max = 50)
    @NotNull
    @Column(name = "logger_name")
    private String loggerName;

    @Basic
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "message")
    private String messageContent;

    /**
     *
     */
    public Log() {
        super();
    }

    /**
     * When logging to the database, the Log object is constructed with a
     *   LogEvent, which is used here to initialize BasicLogEventEntity.
     *   This allows us to collect log attributes, such as level/name/message.
     * @param wrappedEvent
     */
    public Log(LogEvent wrappedEvent) {
        super(wrappedEvent);
        levelName = ""+super.getLevel();
        loggerName = super.getLoggerName();
        Message message = super.getMessage();

        if (message != null){
            Object[] params = message.getParameters();

            if (params!= null) {
                LogDto logDto = (LogDto) params[0];
                messageContent = logDto.getMessage();
                userId = logDto.getUserId();
            } else {
                messageContent = message.getFormattedMessage();
            }
        }
    }

    /**
     *
     * @return logId
     */
    public Long getLogId() {
        return logId;
    }

    /**
     *
     * @param logId
     */
    public void setLogId(Long logId) {
        this.logId = logId;
    }

    /**
     *
     * @return userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     *
     * @return dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     *
     * @param dateCreated
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     *
     * @return loggerName
     */
    public String getLoggerName() {
        return loggerName;
    }

    /**
     *
     * @param loggerName
     */
    public void setLoggerName(String loggerName) {
        this.loggerName = loggerName;
    }

    /**
     *
     * @return levelName
     */
    public String getLevelName() {
        return levelName;
    }

    /**
     *
     * @param levelName
     */
    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    /**
     *
     * @return messageContent
     */
    public String getMessageContent() {
        return messageContent;
    }

    /**
     *
     * @param messageContent
     */
    public void setMessage(String messageContent) {
        this.messageContent = messageContent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (logId != null ? logId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Log other = (Log) obj;
        return Objects.equals(this.logId, other.logId);
    }

    @Override
    public String toString() {
        return "gov.cpsc.itds.ss.domain.Log[ logId=" + logId + ", userId="
                + userId + ", dateCreated=" + dateCreated + ", levelName=" 
                + levelName + ", loggerName=" + loggerName + ", messageContent="
                + messageContent + " ]";
    }
}
