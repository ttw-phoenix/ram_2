package gov.cpsc.itds.ss.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rzauel
 */
@Entity
@Table(name = "system_property", schema = "ram2")
@NamedQueries({
    @NamedQuery(name = "SystemProperty.findAll", query = "SELECT s FROM SystemProperty s"),
    @NamedQuery(name = "SystemProperty.findByPropertyKey", query = "SELECT s FROM SystemProperty s WHERE s.propertyKey = :propertyKey")})
public class SystemProperty implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "property_key")
    private String propertyKey;
    @Size(max = 4000)
    @Column(name = "property_value")
    private String propertyValue;

    /**
     *
     */
    public SystemProperty() {
    }

    /**
     *
     * @param propertyKey
     */
    public SystemProperty(String propertyKey) {
        this.propertyKey = propertyKey;
    }

    /**
     *
     * @return
     */
    public String getPropertyKey() {
        return propertyKey;
    }

    /**
     *
     * @param propertyKey
     */
    public void setPropertyKey(String propertyKey) {
        this.propertyKey = propertyKey;
    }

    /**
     *
     * @return
     */
    public String getPropertyValue() {
        return propertyValue;
    }

    /**
     *
     * @param propertyValue
     */
    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (propertyKey != null ? propertyKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SystemProperty other = (SystemProperty) obj;
        return Objects.equals(this.propertyKey, other.propertyKey);
    }

    @Override
    public String toString() {
        return "gov.cpsc.itds.ss.domain.SystemProperty[ propertyKey=" + propertyKey + " ]";
    }
    
}
