/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.cpsc.itds.ss.dto;

/**
 *
 * @author dlauber
 */
public class LogDto {
    private String userId;
    
    private String message;

    /**
     *
     */
    public LogDto() {
    }

    /**
     *
     * @param userId
     * @param message
     */
    public LogDto(String userId, String message) {
        this.userId = userId;
        this.message = message;
    }
    
    /**
     *
     * @return userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     *
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "LogDto{" + "userId=" + userId + ", message=" + message + "}";
    }
}
