package gov.cpsc.itds.ss.dto;

import java.util.List;

/**
 *
 * @author rzauel
 */
public class SendSupplyChainEmailDto {
    
    private List<String> listEmailAddresses;
    private List<SupplyChainEmailDataDto> listSupplyChainEmailDataDto;

    public SendSupplyChainEmailDto() {
    }

    public SendSupplyChainEmailDto(List<String> listEmailAddresses, List<SupplyChainEmailDataDto> listSupplyChainEmailDataDto) {
        this.listEmailAddresses = listEmailAddresses;
        this.listSupplyChainEmailDataDto = listSupplyChainEmailDataDto;
    }

    public List<String> getListEmailAddresses() {
        return listEmailAddresses;
    }

    public void setListEmailAddresses(List<String> listEmailAddresses) {
        this.listEmailAddresses = listEmailAddresses;
    }

    public List<SupplyChainEmailDataDto> getListSupplyChainEmailDataDto() {
        return listSupplyChainEmailDataDto;
    }

    public void setListSupplyChainEmailDataDto(List<SupplyChainEmailDataDto> listSupplyChainEmailDataDto) {
        this.listSupplyChainEmailDataDto = listSupplyChainEmailDataDto;
    }

    @Override
    public String toString() {
        return "SendSupplyChainEmailDto{" + "listEmailAddresses=" + listEmailAddresses + ", listSupplyChainEmailDataDto=" + listSupplyChainEmailDataDto + '}';
    }

}
