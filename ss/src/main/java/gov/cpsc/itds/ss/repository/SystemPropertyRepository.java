package gov.cpsc.itds.ss.repository;

import gov.cpsc.itds.ss.domain.SystemProperty;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the SystemProperty table.
 */
public interface SystemPropertyRepository extends JpaRepository<SystemProperty,String>{

}
