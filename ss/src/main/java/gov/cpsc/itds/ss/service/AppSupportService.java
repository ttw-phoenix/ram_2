package gov.cpsc.itds.ss.service;

import gov.cpsc.itds.ss.domain.SystemProperty;
import gov.cpsc.itds.ss.repository.SystemPropertyRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author rzauel
 */
@Service
public class AppSupportService {

    private final Logger log = LogManager.getLogger(AppSupportService.class);
    
    /**
     *
     */
    public static final String EMAIL_ACTIVATED = "email_activated";

    /**
     *
     */
    public static final String EMAIL_FROM = "email_from";

    /**
     *
     */
    public static final String EMAIL_HOST = "email_host";

    /**
     *
     */
    public static final String EMAIL_PORT = "email_port";
    
    private final Map<String,String> systemPropertiesMap = new HashMap<>();

    @Inject
    SystemPropertyRepository systemPropertyRepository;

    /**
     *
     */
    @PostConstruct
    public void init() {
        reloadSystemProperties();
    }
    
    /**
     *
     */
    public void reloadSystemProperties() {
        systemPropertiesMap.clear();
        List<SystemProperty> systemPropertyList = systemPropertyRepository.findAll();
        systemPropertyList.stream().forEach((systemProperty) -> {
            systemPropertiesMap.put(systemProperty.getPropertyKey(), systemProperty.getPropertyValue());
        });
    }
    
    /**
     *
     * @param key
     * @return
     */
    public String getSystemPropertyString(String key) {
        return systemPropertiesMap.get(key);
    }

    /**
     *
     * @param key
     * @return
     */
    public Boolean getSystemPropertyBoolean(String key) {
        return Boolean.valueOf(systemPropertiesMap.get(key));
    }

    /**
     *
     * @param key
     * @return
     */
    public Integer getSystemPropertyInteger(String key) {
        return Integer.valueOf(systemPropertiesMap.get(key));
    }

}
