package gov.cpsc.itds.ss.service;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import gov.cpsc.itds.ss.dto.SendSupplyChainEmailDto;
import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * Service class that implements the email methods
 * 
 * @author rzauel
 */
@Service
public class EmailService {

    private final Logger log = LogManager.getLogger(EmailService.class);

    /**
     * Freemarker configuration
     */
    private Configuration cfg;

    /**
     * appSupportService provides the system parameters for connecting to email server
     */
    @Inject
    AppSupportService appSupportService;
    
    /**
     * context is used to get the Freemarker templates from the freemarker-templates directory
     */
    @Inject
    ServletContext context;

    /**
     * method that runs after construction and injection
     */
    @PostConstruct
    public void init() {
        initFreemarker();
    }

    /**
     *
     * Wrapper for sendEmail with exception handling returning a service response
     * 
     * @param toEmailAddress
     * @param subject
     * @param body
     * @param sendHtml
     * @return
     */
    public ServiceResponse<String> sendEmailService(String toEmailAddress, String subject, String body, boolean sendHtml) {
        ServiceResponse<String> response = new ServiceResponse<>();
        try {
            String rtnVal = this.sendEmail(toEmailAddress, subject, body, sendHtml);
            response.setValue(rtnVal);
            log.debug(rtnVal);
        } catch (Exception e) {
            String errorMsg = "error sending email: " + e.getMessage();
            log.error("error sending email", e);
            response.setResponseCode(ServiceResponseCodeEnum.INTERNAL_SERVER_ERROR);
            response.setValue(errorMsg);
        }
        return response;
    }

    /**
     *
     * Basic send email method
     * uses system parameters from appSupportService to connect to email service
     * does not send an email if the system property email_activated is false
     * 
     * @param toEmailAddress - recipient of the email
     * @param subject - subject of the email
     * @param body - body of the email
     * @param sendHtml - true= use HTML email format, false= use plain text email
     * @return String with the message id, toEmailAddress, and subject, or the error message if there was an error.
     * @throws Exception
     */
    public String sendEmail(String toEmailAddress, String subject, String body, boolean sendHtml) throws Exception {
        String rtn;
        String strEmailActivated = appSupportService.getSystemPropertyString(AppSupportService.EMAIL_ACTIVATED);
        boolean isEmailActivated = appSupportService.getSystemPropertyBoolean(AppSupportService.EMAIL_ACTIVATED);
        if (isEmailActivated) {
            Email email;
            if (sendHtml) {
                email = new HtmlEmail();
            } else {
                email = new SimpleEmail();
            }
            email.setHostName(appSupportService.getSystemPropertyString(AppSupportService.EMAIL_HOST));
            email.setSmtpPort(appSupportService.getSystemPropertyInteger(AppSupportService.EMAIL_PORT));
            email.setFrom(appSupportService.getSystemPropertyString(AppSupportService.EMAIL_FROM));
            email.setSubject(subject);
            email.setMsg(body);
            email.addTo(toEmailAddress);
            String msgId = email.send();
            rtn = "{\"MessageId\":\"" + msgId + "\",\"toEmailAddress\":\"" + toEmailAddress + "\",\"subject\":\"" + subject + "\"}";
        } else {
            rtn = "EMAIL_ACTIVATED: " + strEmailActivated + "; Email is turned off";
        }
        return rtn;
    }

    /**
     * method to initialize the Freemarker configuration used for email templates
     */
    private void initFreemarker() {
        // Create your Configuration instance, and specify if up to what FreeMarker
        // version (here 2.3.22) do you want to apply the fixes that are not 100%
        // backward-compatible. See the Configuration JavaDoc for details.
        cfg = new Configuration(Configuration.VERSION_2_3_22);

        try {
            // Specify the source where the template files come from. Here I set a
            // plain directory for it, but non-file-system sources are possible too:
            URL url = context.getResource("/freemarker-templates");
            File dir = new File(url.getPath());
            cfg.setDirectoryForTemplateLoading(dir);
        } catch (Exception e) {
            log.error("error starting initFreemarker.  Unable to load template directory", e);
        }

        // Set the preferred charset template files are stored in. UTF-8 is
        // a good choice in most applications:
        cfg.setDefaultEncoding("UTF-8");

        // Sets how errors will appear.
        // During web page *development* TemplateExceptionHandler.HTML_DEBUG_HANDLER is better.
        // During web page *production* TemplateExceptionHandler.RETHROW_HANDLER is better.
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
    }

    /**
     * Uses supply_chain.ftl as the email template in freemarker-templates directory
     * @param sendSupplyChainEmailDto - holds list of email recipients and list of entry/lines for the content of the email.
     * @return - Service response
     */
    public ServiceResponse<String> sendSupplyChainEmailService(SendSupplyChainEmailDto sendSupplyChainEmailDto) {
        ServiceResponse<String> response = new ServiceResponse<>();
        try {
            if (!sendSupplyChainEmailDto.getListEmailAddresses().isEmpty() && !sendSupplyChainEmailDto.getListSupplyChainEmailDataDto().isEmpty()) {
                Template temp = cfg.getTemplate("supply_chain.ftl");
                Writer out = new StringWriter();
                temp.process(sendSupplyChainEmailDto,out);
                String body = out.toString();
                StringBuilder sb = new StringBuilder();
                for (String toEmailAddress:sendSupplyChainEmailDto.getListEmailAddresses()) {
                    String sendEmailRtn = sendEmail(toEmailAddress, "Stale Entries – Supply Chain", body, true);
                    sb.append(sendEmailRtn).append("; ");
                }
                response.setValue(sb.toString());
            } else {
                response.setResponseCode(ServiceResponseCodeEnum.BAD_REQUEST);
                String errorMsg = "email list or data list is empty";
                response.setValue(errorMsg);
            }
        } catch (Exception e) {
            String errorMsg = "error sending supply chain email: " + e.getMessage();
            log.error("error sending email", e);
            response.setResponseCode(ServiceResponseCodeEnum.INTERNAL_SERVER_ERROR);
            response.setValue(errorMsg);
        }
        return response;
    }

}
