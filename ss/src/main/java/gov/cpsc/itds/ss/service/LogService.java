package gov.cpsc.itds.ss.service;

import gov.cpsc.itds.ss.dto.LogDto;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author rzauel
 */
@Service
public class LogService {
    
    private final Logger log = LogManager.getLogger(LogService.class);
    
    /**
     * Uses log4j to add a custom log to the database
     * @param logDto - the log object being inserted into the database
     * @param level - log level for the new entry
     * @return response - OK or INTERNAL_SERVER_ERROR
     */
    public ServiceResponse<String> addLogService(LogDto logDto, Level level) {
        ServiceResponse<String> response = new ServiceResponse<>();
        try {
            log.log(level, logDto);
            response.setResponseCode(ServiceResponseCodeEnum.OK);
        } catch (Exception e) {
            String errorMsg = "error adding custom log: " + e.getMessage();
            log.error("error adding custom log", e);
            response.setResponseCode(ServiceResponseCodeEnum.INTERNAL_SERVER_ERROR);
            response.setValue(errorMsg);
        }
        return response;
    }
}
