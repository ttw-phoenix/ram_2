package gov.cpsc.itds.ss.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 *
 * Response Object used to hold the response from internal services so they can
 * easily be translated into http Response Entity for response in restful call
 * 
 * @author rzauel
 * @param <T>
 */
public class ServiceResponse<T> {
    
    // defaults response to OK
    private ServiceResponseCodeEnum responseCode = ServiceResponseCodeEnum.OK;
    private T value = null;

    /**
     * Constructor
     */
    public ServiceResponse() {
    }

    /**
     * Constructor with value
     * @param value
     */
    public ServiceResponse(T value) {
        this.value = value;
        this.responseCode = ServiceResponseCodeEnum.OK;
    }

    /**
     * Constructor with response code and value
     * @param responseCode
     * @param value
     */
    public ServiceResponse(ServiceResponseCodeEnum responseCode, T value) {
        this.responseCode = responseCode;
        this.value = value;
    }

    /**
     * Constructor with response code
     * @param responseCode
     */
    public ServiceResponse(ServiceResponseCodeEnum responseCode) {
        this.responseCode = responseCode;
        this.value = null;
    }

    /**
     *
     * @return
     */
    public T getValue() {
        return value;
    }

    /**
     *
     * @param value
     */
    public void setValue(T value) {
        this.value = value;
    }

    /**
     *
     * @return
     */
    public ServiceResponseCodeEnum getResponseCode() {
        return responseCode;
    }

    /**
     *
     * @param responseCode
     */
    public void setResponseCode(ServiceResponseCodeEnum responseCode) {
        this.responseCode = responseCode;
    }
    
    /**
     *
     * @return
     */
    public boolean isOK() {
        return (responseCode == ServiceResponseCodeEnum.OK);
    }

    /**
     * 
     * @return boolean
     */
    public boolean isNotOK() {
        return (responseCode != ServiceResponseCodeEnum.OK);
    }

    /**
     * Translates local Service Response into Http Response Entity
     * @return Http Response Entity
     */
    public ResponseEntity<T> getHttpResponseEntity(){
        HttpStatus httpStatus = HttpStatus.OK;
        switch(this.responseCode) {
            case OK:
                httpStatus = HttpStatus.OK;
                break;
            case BAD_REQUEST:
                httpStatus = HttpStatus.BAD_REQUEST;
                break;
            case INTERNAL_SERVER_ERROR:
                httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
                break;
            case UNAUTHORIZED:
                httpStatus = HttpStatus.UNAUTHORIZED;
                break;
        }
        
        ResponseEntity<T> httpResponseEntity = new ResponseEntity<>(value, httpStatus);
        return httpResponseEntity;
        
    }
    
}
