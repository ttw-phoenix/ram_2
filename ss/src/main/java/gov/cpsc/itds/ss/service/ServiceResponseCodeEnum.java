package gov.cpsc.itds.ss.service;

/**
 *
 * @author rzauel
 */
public enum ServiceResponseCodeEnum {

    /**
     *
     */
    OK,

    /**
     *
     */
    BAD_REQUEST,

    /**
     *
     */
    UNAUTHORIZED,

    /**
     *
     */
    INTERNAL_SERVER_ERROR,

    /**
     *
     */
    NOT_FOUND

}
