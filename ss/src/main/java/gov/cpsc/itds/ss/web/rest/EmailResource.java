package gov.cpsc.itds.ss.web.rest;

import gov.cpsc.itds.ss.dto.SendEmailDto;
import gov.cpsc.itds.ss.dto.SendSupplyChainEmailDto;
import gov.cpsc.itds.ss.service.EmailService;
import gov.cpsc.itds.ss.service.ServiceResponse;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * Restful Resource class which holds the endpoints for rest, unpacks the call, calls the Service class, and responds to the http request.
 * 
 * @author rzauel
 */
@RestController
@RequestMapping("/api/email")
public class EmailResource {

    private final Logger log = LogManager.getLogger(EmailResource.class);

    @Inject
    private EmailService emailService;

    /**
     * POST /api/email/send -> send a single email
     *
     * @param sendEmailDto
     * @return
     */
    @RequestMapping(value = "/send",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> sendEmail(@RequestBody SendEmailDto sendEmailDto) {
        log.debug("REST call to sendEmail");
        System.out.println("test message to see if console works");
        ServiceResponse<String> response = emailService.sendEmailService(sendEmailDto.getToEmailAddress(), sendEmailDto.getSubject(), sendEmailDto.getBody(), sendEmailDto.getSendHtml());
        return response.getHttpResponseEntity();
    }
    
    /**
     * POST /api/email/supplychain -> send an email using the supply chain template
     *
     * @param sendSupplyChainEmailDto
     * @return
     */
    @RequestMapping(value = "/supplychain",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> sendSupplyChainEmail(@RequestBody SendSupplyChainEmailDto sendSupplyChainEmailDto) {
        log.debug("REST call to sendEmail");
        System.out.println("test message to see if console works");
        ServiceResponse<String> response = emailService.sendSupplyChainEmailService(sendSupplyChainEmailDto);
        return response.getHttpResponseEntity();
    }
    
}
