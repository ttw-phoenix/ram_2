package gov.cpsc.itds.ss.web.rest;

import gov.cpsc.itds.ss.dto.LogDto;
import gov.cpsc.itds.ss.service.LogService;
import gov.cpsc.itds.ss.service.ServiceResponse;
import javax.inject.Inject;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author rzauel
 */
@RestController
@RequestMapping("/api/logs")
public class LogResource {

    private final Logger log = LogManager.getLogger(LogResource.class);

    @Inject
    private LogService logService;

    /**
     * POST /debug -> add a new debug log
     *
     * @param logDto
     * @return
     */
    @RequestMapping(value = "/debug",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> addDebugLog(@RequestBody LogDto logDto) {
        log.debug("REST call to addDebugLog");
        ServiceResponse<String> response = logService.addLogService(logDto, Level.DEBUG);
        return response.getHttpResponseEntity();
    }

    /**
     * POST /debug -> add a new error log
     * @param logDto
     * @return
     */
    @RequestMapping(value = "/error",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> addErrorLog(@RequestBody LogDto logDto) {
        log.debug("REST call to addErrorLog");
        ServiceResponse<String> response = logService.addLogService(logDto, Level.ERROR);
        return response.getHttpResponseEntity();
    }

    /**
     * POST /debug -> add a new fatal log
     * @param logDto
     * @return
     */
    @RequestMapping(value = "/fatal",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> addFatalLog(@RequestBody LogDto logDto) {
        log.debug("REST call to addFatalLog");
        ServiceResponse<String> response = logService.addLogService(logDto, Level.FATAL);
        return response.getHttpResponseEntity();
    }

    /**
     * POST /debug -> add a new info log
     * @param logDto
     * @return
     */
    @RequestMapping(value = "/info",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> addInfoLog(@RequestBody LogDto logDto) {
        log.debug("REST call to addInfoLog");
        ServiceResponse<String> response = logService.addLogService(logDto, Level.INFO);
        return response.getHttpResponseEntity();
    }

    /**
     * POST /debug -> add a new warn log
     * @param logDto
     * @return
     */
    @RequestMapping(value = "/warn",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> addWarnLog(@RequestBody LogDto logDto) {
        log.debug("REST call to addWarnLog");
        ServiceResponse<String> response = logService.addLogService(logDto, Level.WARN);
        return response.getHttpResponseEntity();
    }
}
